How to use ArmarX to control iCub via YARP:

1. Start ArmarX: "armarx start && armarx memory start"
2. Start YARP: "yarpserver"
3. Start iCub Simulation: "iCub_SIM"
4. Start ArmarXYarpBridge scenario
5. Start KinematicUnitGui

How to use YARP to control ARMAR-III via ArmarX

1. Start ArmarX: "armarx start && armarx memory start"
2. Start YARP: "yarpserver"
3. Start Armar3Simulation scenario
4. Start YarpArmarXBridge scenario

How to run cartesian controller on Armar3 robot: 

1- run the YarpArmarxBridge
2- $ simCartesianControl --name armarx --robot armarx --context ArmarIIICartesianSolver --left_arm_file cartesianLeftArm.ini --right_arm_file cartesianRightArm.ini --no_legs
3- $ iKinCartesianSolver --context ArmarIIICartesianSolver --part left_arm
4- $ iKinCartesianSolver --context ArmarIIICartesianSolver --part right_arm
5- you can test by running cartesianInterfaceExample:
   $ cartesianInterfaceExample --robot ArmarIII --part left_arm --onlyXYZ


