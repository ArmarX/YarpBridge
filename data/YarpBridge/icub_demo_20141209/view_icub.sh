#!/bin/bash

shopt -s nullglob
for f in *_iCub.rob.xml
do
	echo "viewing: $f"
	../../../../../mmmtools/build/bin/MMMViewer --motion $f &
done
