#!/bin/bash

#shopt -s nullglob
FILES=*.mmm.xml
#FILES="nordic_walking03.mmm.xml"

for f in $FILES
do
	for id in "initialsln_iCub"
	do
		echo "working on: $f with $id."

		../../../../../mmmtools/build/bin/MMMConverter \
		--converter ConverterMMM2RobX \
		--converterConfigFile mmm_$id.config.xml \
		--outputModel ../../Model/iCub_new/iCub.xml \
		--libPath ../../../converterMMM2RobX/build/lib/ \
		--inputModel ../../Model/Winter/mmm.xml \
		--inputDataMMM $f \
		--outputFile $f.$id.rob.xml
	done
done
