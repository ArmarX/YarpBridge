#include <RobotAPI/interface/core/FramedPoseBase.ice>

module armarx
{

    interface TCPPoseControlUnitInterface
    {
        void setTCPTarget(string robotNodeSetName, string tcpNodeName, FramedPositionBase targetPosition, FramedOrientationBase targetOrientation, int timeForMotionInMS);
        void stopTCP(string robotNodeSetName, string tcpNodeName);
    };

};
