/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    YarpBridge::gui-plugins::YarpActionExecutionWidgetController
 * \author     Markus Grotz ( markus dot grotz at kit dot edu )
 * \date       2016
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "YarpActionExecutionWidgetController.h"

#include <string>

using namespace armarx;

YarpActionExecutionWidgetController::YarpActionExecutionWidgetController()
{
    widget.setupUi(getWidget());

    connect(widget.resetButton, SIGNAL(clicked()), this, SLOT(resetState()));
    connect(widget.nextButton, SIGNAL(clicked()), this, SLOT(nextState()));
}


YarpActionExecutionWidgetController::~YarpActionExecutionWidgetController()
{
    QObject::disconnect(this, SLOT(resetState()));
    QObject::disconnect(this, SLOT(nextState()));

}


void YarpActionExecutionWidgetController::loadSettings(QSettings* settings)
{

}

void YarpActionExecutionWidgetController::saveSettings(QSettings* settings)
{

}


void YarpActionExecutionWidgetController::onInitComponent()
{
    usingProxy("YarpActionExecution");
}


void YarpActionExecutionWidgetController::onConnectComponent()
{
    actionExecution = getProxy<ActionExecutionInterfacePrx>("YarpActionExecution");

    std::string currentStateName = "";// actionExecution->getCurrentState();

    widget.currentStateLabel->setText(QString::fromStdString(currentStateName));

}


void YarpActionExecutionWidgetController::resetState()
{
    actionExecution->resetState();

    widget.nextButton->setEnabled(true);
}


void YarpActionExecutionWidgetController::nextState()
{
    bool hasNext = actionExecution->nextState();

    widget.nextButton->setEnabled(hasNext);

    std::string currentStateName = ""; //actionExecution->getCurrentState();

    widget.currentStateLabel->setText(QString::fromStdString(currentStateName));
}
