armarx_set_target("YarpActionExecutionGuiPlugin")

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

set(SOURCES YarpActionExecutionGuiPlugin.cpp YarpActionExecutionWidgetController.cpp)
set(HEADERS YarpActionExecutionGuiPlugin.h   YarpActionExecutionWidgetController.h)

set(GUI_MOC_HDRS ${HEADERS})
set(GUI_UIS YarpActionExecutionWidget.ui)

set(COMPONENT_LIBS WalkManInterfaces ArmarXCore ${Simox_LIBRARIES})

if(ArmarXGui_FOUND)
    armarx_gui_library(YarpActionExecutionGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
