/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::gui-plugins::YarpActionExecutionWidgetController
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_YarpBridge_YarpActionExecution_WidgetController_H
#define _ARMARX_YarpBridge_YarpActionExecution_WidgetController_H

#include "ui_YarpActionExecutionWidget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <WalkMan/interface/ActionExecutionInterface.h>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-YarpActionExecution YarpActionExecution
    \brief The YarpActionExecution allows visualizing ...

    \image html YarpActionExecution.png
    The user can

    API Documentation \ref YarpActionExecutionWidgetController

    \see YarpActionExecutionGuiPlugin
    */

    /**
     * \class YarpActionExecutionWidgetController
     * \brief YarpActionExecutionWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        YarpActionExecutionWidgetController:
    public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit YarpActionExecutionWidgetController();

        /**
         * Controller destructor
         */
        virtual ~YarpActionExecutionWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings);

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings);

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        virtual QString getWidgetName() const
        {
            return "YarpActionExecution";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent() override;

    public slots:

        void resetState();

        void nextState();

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::YarpActionExecutionWidget widget;

        ActionExecutionInterfacePrx actionExecution;


    };
}

#endif
