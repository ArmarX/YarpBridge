/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpImplPostionControl
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarxMotorControl.h"
#include "YarpMotorControlHelper.h"

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <RobotAPI/components/units/KinematicUnitObserver.h>

using namespace armarx;
using namespace yarp::os;
using namespace yarp::dev;
using namespace std;


bool YarpMotorControlHelper::getInteractionMode(int axis, yarp::dev::InteractionModeEnum* mode)
{
    *mode =  VOCAB_IM_STIFF;
    return true;
}

bool YarpMotorControlHelper::getInteractionModes(int n_joints, int *joints, yarp::dev::InteractionModeEnum* modes)
{
    try{
        for(int i=0; i<n_joints; i++)
            modes[joints[i]] =  VOCAB_IM_STIFF;        
    }
    catch(...) {
        handleExceptions();
    }
    return true;
}

bool YarpMotorControlHelper::getInteractionModes(yarp::dev::InteractionModeEnum* modes)
{
    try {
        for(unsigned int i=0; i<numberOfJoints; i++)
             modes[i] =  VOCAB_IM_STIFF;
    }
    catch(...) {
        handleExceptions();
    }
    return true;
}

bool YarpMotorControlHelper::setInteractionMode(int axis, yarp::dev::InteractionModeEnum mode)
{
    return (mode == VOCAB_IM_STIFF);
}

bool YarpMotorControlHelper::setInteractionModes(int n_joints, int *joints, yarp::dev::InteractionModeEnum* modes)
{
    try {
        for(int i=0; i<n_joints; i++)
            if(modes[joints[i]] != VOCAB_IM_STIFF)
                return false;
    }
    catch(...) {
        handleExceptions();
    }
    return true;
}

bool YarpMotorControlHelper::setInteractionModes(yarp::dev::InteractionModeEnum* modes)
{
    try {
        for(unsigned int i=0; i<numberOfJoints; i++)
            if(modes[i] != VOCAB_IM_STIFF)
                return false;
    }
    catch(...) {
        handleExceptions();
    }
    return true;
}

