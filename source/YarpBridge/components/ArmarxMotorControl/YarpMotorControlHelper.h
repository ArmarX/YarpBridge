/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpMotorControlHelper
 * @author     Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpArmarxBridge_YarpMotorControlHelper_H
#define _ARMARX_COMPONENT_YarpArmarxBridge_YarpMotorControlHelper_H

#include <yarp/sig/all.h>
//#include <yarp/os/all.h>
#include <yarp/os/Network.h>
#include <yarp/os/Property.h>
#include <yarp/dev/Drivers.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/dev/ControlBoardInterfaces.h>
#include <yarp/dev/IControlMode.h>
#include <yarp/sig/Vector.h>
#include <yarp/os/Time.h>
#include <yarp/dev/IControlLimits2.h>
#include <yarp/dev/IPositionControl.h>
#include <yarp/dev/IPositionDirect.h>
#include <yarp/dev/IInteractionMode.h>
#include <yarp/dev/IControlMode2.h>
#include <yarp/dev/Wrapper.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <map>

namespace armarx
{

    /**
     * @class YarpMotorControlHelper
     * @brief A brief description
     *
     * Detailed Description
     */
    class YarpMotorControlHelper :
        public yarp::dev::DeviceDriver,
        public yarp::dev::IControlLimits2,
        public yarp::dev::IPositionControl2,
        public yarp::dev::IVelocityControl,
        public yarp::dev::IEncodersTimed,
        public yarp::dev::IControlMode,
        public yarp::dev::IInteractionMode,
        public yarp::dev::IPositionDirect,
        public Logging

    {
    public:
        YarpMotorControlHelper();
        virtual ~YarpMotorControlHelper();
       
        /*
         * Implelenting YARP device 
         */
        virtual bool open(yarp::os::Searchable& config);

        virtual bool close(void);

        /*
         * IControlLimits2
         */
        bool  setVelLimits (int axis, double min, double max)   {return false;}
        bool  getVelLimits (int axis, double *min, double *max) {return false;}
        bool  setLimits (int axis, double min, double max)      {return false;}
        bool  getLimits (int axis, double *min, double *max);

        /*
         * Implementing IPositionControl
         */
        virtual bool stop(int j); //WORKS
        virtual bool stop(); //WORKS
        virtual bool positionMove(int j, double ref); //WORKS
        virtual bool getAxes(int *ax); // WORKS
        virtual bool positionMove(const double *refs); //WORKS
        virtual bool setRefSpeed(int j, double sp); //WORKS
        virtual bool getRefSpeed(int j, double *ref); //WORKS
        virtual bool getRefSpeeds(double *spds); //WORKS

        virtual bool relativeMove(int j, double delta); //NOT TESTED
        virtual bool relativeMove(const double *deltas); //NOT TESTED
        virtual bool checkMotionDone(int j, bool *flag); //NOT TESTED
        virtual bool checkMotionDone(bool *flag); //NOT TESTED
        virtual bool setPositionMode(); //NOT TESTED
                
        /*
         * Implementing IPositionControl2
         */        
        virtual bool positionMove(const int n_joint, const int *joints, const double *refs);
        virtual bool relativeMove(const int n_joint, const int *joints, const double *deltas);
        virtual bool checkMotionDone(const int n_joint, const int *joints, bool *flags);
        virtual bool setRefSpeeds(const int n_joint, const int *joints, const double *spds);
        virtual bool setRefAccelerations(const int n_joint, const int *joints, const double *accs);
        virtual bool getRefSpeeds(const int n_joint, const int *joints, double *spds);
        virtual bool getRefAccelerations(const int n_joint, const int *joints, double *accs);
        virtual bool stop(const int n_joint, const int *joints);

        virtual bool setRefSpeeds(const double *spds); //NOT TESTED
    
        virtual bool setRefAcceleration(int j, double acc); //NOT IMPLEMENTED
        virtual bool setRefAccelerations(const double *accs); //NOT IMPLEMENTED
        virtual bool getRefAcceleration(int j, double *acc); //NOT IMPLEMENTED
        virtual bool getRefAccelerations(double *accs); //NOT IMPLEMENTED

        
        /*
         * Implelenting IPositionDirect
         */

  
        virtual bool setPositionDirectMode();
        virtual bool setPosition(int j, double ref);
        virtual bool setPositions(const int n_joint, const int *joints, double *refs);
        virtual bool setPositions(const double *refs);


        /*
         * Implelenting IVelocityControl
         */
        virtual bool setVelocityMode();
        virtual bool velocityMove(int j, double sp);
        virtual bool velocityMove(const double *sp);

        /*
         * Implelenting IEncodersTimed
         */ 
        virtual bool getEncoder(int j, double *v);
        virtual bool getEncoders(double *encs);
        virtual bool resetEncoder(int j);
        virtual bool resetEncoders();
        virtual bool setEncoder(int j, double val);
        virtual bool setEncoders(const double *vals);

        virtual bool getEncoderSpeed(int j, double *sp);
        virtual bool getEncoderSpeeds(double *spds);

        virtual bool getEncoderAcceleration(int j, double *spds);
        virtual bool getEncoderAccelerations(double *accs);

        // ENCODERS TIMED
        virtual bool getEncodersTimed(double *encs, double *time);
        virtual bool getEncoderTimed(int j, double *encs, double *time);


       /**
        * Implelenting  IInteractionMode
        */ 
        virtual bool getInteractionMode(int axis, yarp::dev::InteractionModeEnum* mode);
        virtual bool getInteractionModes(int n_joints, int *joints, yarp::dev::InteractionModeEnum* modes);
        virtual bool getInteractionModes(yarp::dev::InteractionModeEnum* modes);
        virtual bool setInteractionMode(int axis, yarp::dev::InteractionModeEnum mode);
        virtual bool setInteractionModes(int n_joints, int *joints, yarp::dev::InteractionModeEnum* modes);
        virtual bool setInteractionModes(yarp::dev::InteractionModeEnum* modes);

    private:
        bool bPositionDirect;

        yarp::dev::PolyDriver* wrap;

        KinematicUnitInterfacePrx kinUnitInterface;        // send commands to kinematic unit

        std::string robotNodeSetName;
        unsigned int numberOfJoints;
        yarp::sig::Vector desiredPosition;
    
        yarp::sig::Vector pos;
        yarp::sig::VectorOf<int> encoder;

        /*
        yarp::sig::VectorOf<int> jointID_map;
        yarp::sig::VectorOf<int> motorID_map;
        yarp::sig::VectorOf<int> zero;
        yarp::sig::Vector max_pos, min_pos;
        double simu_time;
        yarp::sig::VectorOf<int> controlMode;

        //Contains the parameters of the device contained in the yarpConfigurationFile .ini file
        yarp::sig::Vector zeroPosition;

        yarp::sig::Vector vel, speed, acc, amp, torque, current;
        yarp::os::Semaphore pos_lock;
        yarp::sig::Vector referenceSpeed, referencePosition, referenceAcceleraton, referenceTorque;
        */

        // yarp::dev::IControlMode interface
    public:
        bool setPositionMode(int j);
        bool setVelocityMode(int j);
        bool setTorqueMode(int j);
        bool setImpedancePositionMode(int j);
        bool setImpedanceVelocityMode(int j);
        bool setOpenLoopMode(int j);
        bool getControlMode(int j, int *mode);
        bool getControlModes(int *modes);
        //bool getControlModes(const int n_joint, const int *joints, int *modes);
        //bool setControlMode(const int j, const int mode);
        //bool setControlModes(const int n_joint, const int *joints, int *modes);
        //bool setControlModes(int *modes);
    };
}

#endif
