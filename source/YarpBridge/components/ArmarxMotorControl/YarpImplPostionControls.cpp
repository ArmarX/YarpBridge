/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpImplPostionControl
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarxMotorControl.h"
#include "YarpMotorControlHelper.h"

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <RobotAPI/components/units/KinematicUnitObserver.h>

using namespace armarx;
using namespace yarp::os;
using namespace yarp::dev;
using namespace std;


bool YarpMotorControlHelper::positionMove(int j, double ref) //WORKS
{

   // ARMARX_INFO << deactivateSpam() << " motionControl: positionMove " << std::endl;
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();

    NameValueMap joints;
    joints[name] = ref/180*M_PI;
    NameControlModeMap modes;
    modes[name] = ePositionControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointAngles(joints);
    return true;

}

bool YarpMotorControlHelper::positionMove(const double *refs) 
{
    ARMARX_INFO << deactivateSpam() << " motionControl: positionMove multi" << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        joints[node->getName()] = refs[i]/180*M_PI;
        modes[node->getName()] = ePositionControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointAngles(joints);
    return true;
}

bool YarpMotorControlHelper::getAxes(int *ax) //WORKS
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getAxes " << std::endl;

    *ax = numberOfJoints;
    return true;
}

bool YarpMotorControlHelper::setRefSpeed(int j, double sp) 
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefSpeed " << std::endl;
    // ARMARX_INFO << deactivateSpam() << " motionControl: positionMove " << std::endl;
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();

    NameValueMap joints;
    joints[name] = sp/180.0*M_PI;
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}

bool YarpMotorControlHelper::getRefSpeed(int j, double *ref) 
{
    try
    {
        ARMARX_INFO << deactivateSpam(3) << " motionControl: getRefSpeed " << std::endl;
        std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();
        DataFieldIdentifierPtr id = new DataFieldIdentifier(ArmarxMotorControl::getKinematicUnitObserver()->ice_getIdentity().name, "jointvelocities", name);
        auto v = ArmarxMotorControl::getKinematicUnitObserver()->getDataField(id);
        if(v->getInitialized())
            *ref = v->getFloat() * 180.0/M_PI;
        else
        {
            *ref = 0.0;
            ARMARX_WARNING << name << " was not initialized";
        }
    }
    catch(...)
    {
        handleExceptions();
    }
    return true;
}

bool YarpMotorControlHelper::getRefSpeeds(double *spds) 
{
    try
    {
        ARMARX_INFO << deactivateSpam() << " motionControl: getRefSpeeds " << std::endl;
        auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();
        NameList jointNames;
        DataFieldIdentifierBaseList idList;
        for(auto node:nodes)
        {
            jointNames.push_back(node->getName());
            DataFieldIdentifierPtr id = new DataFieldIdentifier(ArmarxMotorControl::getKinematicUnitObserver()->ice_getIdentity().name, "jointvelocities", node->getName());
            idList.push_back(id);
        }


        TimedVariantBaseList variants = ArmarxMotorControl::getKinematicUnitObserver()->getDataFields(idList);
        int i = 0;
        for( TimedVariantBasePtr v : variants)
        {
            if(v->getInitialized())
                spds[i] = v->getFloat() * 180.0/M_PI;
            else
            {
                spds[i] = 0.0;
                ARMARX_WARNING << nodes[i]->getName() << " was not initialized";
            }
            i++;
        }
    }
    catch(...)
    {
        handleExceptions();
    }
    return true;
}

bool YarpMotorControlHelper::relativeMove(int j, double delta) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: relativeMove " << std::endl;
    return false;
}

bool YarpMotorControlHelper::relativeMove(const double *deltas) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: relativeMove " << std::endl;
    return false;
}

bool YarpMotorControlHelper::checkMotionDone(int j, bool *flag) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam(3) << " motionControl: checkMotionDone " << std::endl;
    return true;
}

bool YarpMotorControlHelper::checkMotionDone(bool *flag) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: checkMotionDone " << std::endl;
    return true;
}

bool YarpMotorControlHelper::setPositionMode() //NOT TESTED
{
    bPositionDirect = false;
    ARMARX_INFO << deactivateSpam() << " motionControl: setPositionMode " << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        modes[node->getName()] = ePositionControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool YarpMotorControlHelper::setRefSpeeds(const double *spds) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefSpeeds " << std::endl;

    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    int i  = 0;
    for(auto node : nodes)
    {
        joints[node->getName()] = spds[i]/180*M_PI;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}


bool YarpMotorControlHelper::setRefAcceleration(int j, double acc) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefAcceleration " << std::endl;
    return false;
}

bool YarpMotorControlHelper::setRefAccelerations(const double *accs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefAccelerations " << std::endl;
    return false;
}

bool YarpMotorControlHelper::getRefAcceleration(int j, double *acc) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getRefAcceleration " << std::endl;
    return false;
}

bool YarpMotorControlHelper::getRefAccelerations(double *accs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getRefAccelerations " << std::endl;
    return false;
}


bool YarpMotorControlHelper::positionMove(const int n_joint, const int *jnts, const double *refs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: positionMove(n, j, r) " << std::endl;

    for(int i=0; i<n_joint; i++)
    {            
        std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(jnts[i])->getName();
        NameValueMap joints;
        joints[name] = refs[i]/180*M_PI;
        NameControlModeMap modes;
        modes[name] = ePositionControl;
        ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
        ArmarxMotorControl::getKinematicUnitInterface()->setJointAngles(joints);
    }
    return true;
}

bool YarpMotorControlHelper::relativeMove(const int n_joint, const int *joints, const double *deltas) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: relativeMove(n, j, r) " << std::endl;
    return false;
}


bool YarpMotorControlHelper::checkMotionDone(const int n_joint, const int *joints, bool *flags) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: checkMotionDone(n, j, r) " << std::endl;
    return true;
}

bool YarpMotorControlHelper::setRefSpeeds(const int n_joint, const int *joints, const double *spds) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefSpeeds(n, j, r) " << std::endl;
    return false;
}


bool YarpMotorControlHelper::setRefAccelerations(const int n_joint, const int *joints, const double *accs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setRefAccelerations(n, j, r) " << std::endl;
    return false;
}


bool YarpMotorControlHelper::getRefSpeeds(const int n_joint, const int *joints, double *spds) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getRefSpeeds(n, j, r) " << std::endl;
    return false;
}


bool YarpMotorControlHelper::getRefAccelerations(const int n_joint, const int *joints, double *accs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getRefAccelerations(n, j, r) " << std::endl;
    return false;
}


bool YarpMotorControlHelper::stop(const int n_joint, const int *jnts) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: stop(n, j) " << std::endl;
    
    for(int i=0; i<n_joint; i++)
    {
        std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(jnts[i])->getName();
        NameValueMap joints;
        joints[name] = 0.0;
        NameControlModeMap modes;
        modes[name] = eVelocityControl;
        ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
        ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    }

    return true;
}


bool YarpMotorControlHelper::setPositionDirectMode()
{
    bPositionDirect = true;
    ARMARX_INFO << deactivateSpam() << " motionControl: setPositionDirectMode " << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        modes[node->getName()] = ePositionControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool YarpMotorControlHelper::setPosition(int j, double ref)
{
    return positionMove(j, ref);
}

bool YarpMotorControlHelper::setPositions(const int n_joint, const int *joints, double *refs)
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setPositions(n, j, r) " << std::endl;
    return positionMove(n_joint, joints, refs);
}

bool YarpMotorControlHelper::setPositions(const double *refs)
{
    return positionMove(refs);
}

