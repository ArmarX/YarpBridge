/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpMotorControlHelper
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarxMotorControl.h"
#include "YarpMotorControlHelper.h"

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <RobotAPI/components/units/KinematicUnitObserver.h>

using namespace armarx;
using namespace yarp::os;
using namespace yarp::dev;
using namespace std;

YarpMotorControlHelper::YarpMotorControlHelper() 
{
    kinUnitInterface = ArmarxMotorControl::getKinematicUnitInterface();
}

YarpMotorControlHelper::~YarpMotorControlHelper()
{

}

bool YarpMotorControlHelper::open(yarp::os::Searchable& config)
{    
    numberOfJoints = config.find("joints").asInt();
    robotNodeSetName = config.find("robotNodeSetName").asString();
    pos.resize(numberOfJoints);
    encoder.resize(numberOfJoints);

    string partName, robotName, wholeName;
    yarp::dev::IMultipleWrapper* iWrap;
    wrap = new yarp::dev::PolyDriver();

    yarp::os::Property wrapProp;
    wrapProp.fromString(config.toString());
    partName = config.find("part_name").asString();
    robotName = config.find("robot").asString();

    wrapProp.unput("device");
    wrapProp.put("device", "controlboardwrapper2");

    wrapProp.unput("name");
    wholeName = robotName + string("/") + partName;
    wrapProp.put("name", wholeName.c_str());

    ARMARX_INFO << deactivateSpam() << "robotName is " << robotName << "; partName is " << partName << std::endl;
    ARMARX_INFO << deactivateSpam() << "\n*********\n before wrapper " << wrapProp.toString() << "\n***********\n" << std::endl;

    wrap->open(wrapProp);
    if (!wrap->isValid())
        fprintf(stderr, "YarpMotorControlHelper: wrapper did not open\n");
    else
        fprintf(stderr, "YarpMotorControlHelper: wrapper opened correctly\n");

    if (!wrap->view(iWrap)) {
        printf("YarpMotorControlHelper Wrapper interface not found\n");
    }
   
    yarp::dev::PolyDriverList polyList;

    polyList.push((yarp::dev::PolyDriver*) this, partName.c_str() );
    if(!iWrap->attachAll(polyList) )
    {
       ARMARX_ERROR << deactivateSpam() << "\n\n\nERROR while attching\n\n\n" << std::endl;
       return false;
    }
    else
    {
       ARMARX_INFO << deactivateSpam() << "\n ATTACH WAS OK\n" << std::endl;
    }

    //ImplementPositionControl<YarpMotorControlHelper, IPositionControl>::
    //    initialize(njoints, axisMap, angleToEncoder, zeros);


    setPositionMode();

    return true;
}

bool YarpMotorControlHelper::close(void)
{
    ARMARX_INFO << deactivateSpam() << "YarpMotorControlHelper::close" << std::endl;
    if(wrap)
        wrap->close();
    return true;
}

bool YarpMotorControlHelper::getLimits (int axis, double *min, double *max) 
{
    *min = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(axis)->getJointLimitLow()*180/M_PI;
    *max = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(axis)->getJointLimitHigh()*180/M_PI;
    ARMARX_INFO << deactivateSpam() << VAROUT(*min) << VAROUT(*max);
    return true;
}




// IVelocityControl
bool YarpMotorControlHelper::setVelocityMode()
{
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        modes[node->getName()] = eVelocityControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool YarpMotorControlHelper::velocityMove(int j, double sp) 
{
    ARMARX_INFO << deactivateSpam() << " velocityMove(j, sp) " << std::endl;
    // ARMARX_INFO << deactivateSpam() << " motionControl: positionMove " << std::endl;
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();

    NameValueMap joints;
    joints[name] = sp/180.0*M_PI;
    NameControlModeMap modes;
    modes[name] = eVelocityControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}


bool YarpMotorControlHelper::velocityMove(const double *sp)
{
    ARMARX_INFO << deactivateSpam() << " velocityMove(sp) " << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        joints[node->getName()] = sp[i]/180*M_PI;
        modes[node->getName()] = eVelocityControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}


bool armarx::YarpMotorControlHelper::setPositionMode(int j)
{
    
    bPositionDirect = false;    
    ARMARX_INFO << deactivateSpam(3) << "setPositionMode";
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();
    NameControlModeMap modes;
    modes[name] = ePositionControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool armarx::YarpMotorControlHelper::setVelocityMode(int j)
{
    ARMARX_INFO << deactivateSpam(3) << "setVelocityMode(j)";
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();
    NameControlModeMap modes;
    modes[name] = eVelocityControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool armarx::YarpMotorControlHelper::setTorqueMode(int j)
{
    ARMARX_INFO << deactivateSpam(3) << "setTorqueMode";
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();
    NameControlModeMap modes;
    modes[name] = eTorqueControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    return true;
}

bool armarx::YarpMotorControlHelper::setImpedancePositionMode(int j)
{
    ARMARX_INFO << deactivateSpam() << "NYI";
    return false;
}

bool armarx::YarpMotorControlHelper::setImpedanceVelocityMode(int j)
{
    ARMARX_INFO << deactivateSpam() << "NYI";
    return false;
}

bool armarx::YarpMotorControlHelper::setOpenLoopMode(int j)
{
    ARMARX_INFO << deactivateSpam() << "NYI";
    return false;
}

bool armarx::YarpMotorControlHelper::getControlMode(int j, int *mode)
{
    try{
        ARMARX_INFO << deactivateSpam(3) << "getControlMode " << std::endl;
        std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();
        DataFieldIdentifierPtr id = new DataFieldIdentifier(ArmarxMotorControl::getKinematicUnitObserver()->ice_getIdentity().name, "jointcontrolmodes", name);
        std::string modeStr;
        VariantBasePtr v = ArmarxMotorControl::getKinematicUnitObserver()->getDataField(id);
        if(v)
            modeStr = v->getString();
        ControlMode modeValue = KinematicUnitObserver::StringToControlMode(modeStr);
        if(modeValue == ePositionControl) {
            *mode = (bPositionDirect) ? VOCAB_CM_POSITION_DIRECT : VOCAB_CM_POSITION;
            ARMARX_INFO << deactivateSpam(3) << "VOCAB_CM_POSITION" << std::endl;
        }
        else if(modeValue == eVelocityControl) {
            ARMARX_INFO << deactivateSpam(3) << "VOCAB_CM_VELOCITY" << std::endl;
            *mode = VOCAB_CM_VELOCITY;
        }    
        else if(modeValue == eTorqueControl) {
            ARMARX_INFO << deactivateSpam(3) << "VOCAB_CM_TORQUE" << std::endl;
            *mode = VOCAB_CM_TORQUE;
        }            
        else {
            ARMARX_INFO << deactivateSpam(3) << "VOCAB_CM_UNKNOWN" << std::endl;
            *mode = VOCAB_CM_UNKNOWN;
        }    
    }
    catch(...)
    {
        handleExceptions();
    }

    return true;

}

/*
//
// IControlMode2
//

bool YarpMotorControlHelper::getControlModes(const int n_joint, const int *joints, int *modes)
{
}

bool YarpMotorControlHelper::setControlMode(const int j, const int mode)
{
}

bool YarpMotorControlHelper::setControlModes(const int n_joint, const int *joints, int *modes)
{

}

bool YarpMotorControlHelper::setControlModes(int *modes)
{

}

*/
//
// implelmenting stop and stop(j) from IPositionControl and IVelocityControl
//
bool YarpMotorControlHelper::stop(int j)
{
    ARMARX_INFO << deactivateSpam() << " motionControl: stop(j) " << std::endl;
    std::string name = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getNode(j)->getName();

    //ArmarxMotorControl::getKinematicUnitInterface()->stop();
         
    // getKinematicUnitInterface()->stop() should be enough to stop the robot
    // but to ensure that stop() works also for the robot interfaces which does
    // not support the stop() command we set the velocity to zero too.
    NameValueMap joints;
    joints[name] = 0.0;
    NameControlModeMap modes;
    modes[name] = eVelocityControl;
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}

bool YarpMotorControlHelper::stop()
{
    ARMARX_INFO << deactivateSpam() << " motionControl: stop() " << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();

    NameValueMap joints;
    NameControlModeMap modes;
    int i  = 0;
    for(auto node : nodes)
    {
        joints[node->getName()] = 0.0;
        modes[node->getName()] = eVelocityControl;
        i++;
    }
    ArmarxMotorControl::getKinematicUnitInterface()->switchControlMode(modes);
    ArmarxMotorControl::getKinematicUnitInterface()->setJointVelocities(joints);
    return true;
}


bool armarx::YarpMotorControlHelper::getControlModes(int *modes)
{
    ARMARX_INFO << deactivateSpam(3) << "getControlModes " << std::endl;
    auto nodes = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getAllRobotNodes();
    NameList jointNames;
    DataFieldIdentifierBaseList idList;
    for(auto node:nodes)
    {
        jointNames.push_back(node->getName());
        DataFieldIdentifierPtr id = new DataFieldIdentifier(ArmarxMotorControl::getKinematicUnitObserver()->ice_getIdentity().name, "jointcontrolmodes", node->getName());
        idList.push_back(id);
    }


    TimedVariantBaseList variants = ArmarxMotorControl::getKinematicUnitObserver()->getDataFields(idList);
    int i = 0;
    for(auto v : variants)
    {
        std::string modeStr;
        if(v)
            modeStr = v->getString();
        ControlMode modeValue = KinematicUnitObserver::StringToControlMode(modeStr);
        if(modeValue == ePositionControl)
            modes[i] = VOCAB_CM_POSITION;
        else if(modeValue == eVelocityControl)
            modes[i] = VOCAB_CM_VELOCITY;
        else if(modeValue == eTorqueControl)
            modes[i] = VOCAB_CM_TORQUE;
        else
            modes[i] = VOCAB_CM_UNKNOWN;
        i++;

    }
    return true;
}
