/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpMotorControlHelper
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarxMotorControl.h"
#include "YarpMotorControlHelper.h"

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <RobotAPI/components/units/KinematicUnitObserver.h>

using namespace armarx;
using namespace yarp::os;
using namespace yarp::dev;
using namespace std;


/////////////////////////////////////
// ENCODER
/////////////////////////////////////

bool YarpMotorControlHelper::getEncoder(int j, double *v) //TO BE TESTED
{

    if (v && j >= 0 && j < (int)numberOfJoints) {
//        NameList names = ArmarxMotorControl::getRobotStateComponentInterface()->getSynchronizedRobot()->getRobotNodeSet(robotNodeSetName)->names;
        auto jointValues = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getJointValues();
        if(j >= (int)jointValues.size())
        {
            ARMARX_WARNING_S << "joint index is bigger than namelist " << j << " size: " << jointValues.size();
            return false;
        }
        ARMARX_INFO << deactivateSpam() << "getting encoder for " << j << ": "  << (jointValues.at(j) * 180.0/M_PI);
        *v = jointValues.at(j) * 180.0/M_PI;
//        * v = ArmarxMotorControl::getRobotStateComponentInterface()->getSynchronizedRobot()->getRobotNode(names.at(j))->getJointValue();

        return true;
    }
    return false;
}

bool YarpMotorControlHelper::getEncoders(double *encs) //TO BE TESTED
{

    if (!encs) return false;
    auto jointValues = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getJointValues();
    for (unsigned int i = 0; i < jointValues.size(); ++i)
    {
        //ARMARX_INFO << deactivateSpam() << "getting encoders " << jointValues[i] * 180.0/M_PI;
        encs[i] = jointValues[i] * 180.0/M_PI;  //should we just use memcopy here?
    }
    return true;
}

bool YarpMotorControlHelper::getEncodersTimed(double *encs, double *time) //TO BE TESTED
{
    if (!encs) return false;
    auto jointValues = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getJointValues();
    for (unsigned int i = 0; i < jointValues.size(); ++i)
    {
        //ARMARX_INFO << deactivateSpam() << "getting encoders " << jointValues[i] * 180.0/M_PI;
        encs[i] = jointValues[i] * 180.0/M_PI;  //should we just use memcopy here?
    }
    *time = Time::now();
    return true;
}


bool YarpMotorControlHelper::getEncoderTimed(int j, double *enc, double *time) //TO BE TESTED
{
    if (enc && time && j >= 0 && j < (int)numberOfJoints) {
//        NameList names = ArmarxMotorControl::getRobotStateComponentInterface()->getSynchronizedRobot()->getRobotNodeSet(robotNodeSetName)->names;
        auto jointValues = ArmarxMotorControl::getLocalRobot()->getRobotNodeSet(robotNodeSetName)->getJointValues();
        if(j >= (int)jointValues.size())
        {
            ARMARX_WARNING_S << "joint index is bigger than namelist " << j << " size: " << jointValues.size();
            return false;
        }
        //ARMARX_INFO << deactivateSpam() << "getting encoder for " << j << ": "  << (jointValues.at(j) * 180.0/M_PI);
        *enc = jointValues.at(j) * 180.0/M_PI;
        *time = Time::now();
        return true;
    }
    return false;
}


bool YarpMotorControlHelper::resetEncoder(int j) //TO BE DONE
{
    ARMARX_INFO << deactivateSpam() << " motionControl: resetEncoder " << std::endl;
    return false;
}

bool YarpMotorControlHelper::resetEncoders() //TO BE DONE
{
    ARMARX_INFO << deactivateSpam() << " motionControl: resetEncoders " << std::endl;
    return false;
}

bool YarpMotorControlHelper::setEncoder(int j, double val) //TO BE DONE
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setEncoder " << std::endl;
    return false;
}

bool YarpMotorControlHelper::setEncoders(const double *vals) //TO BE DONE
{
    ARMARX_INFO << deactivateSpam() << " motionControl: setEncoders " << std::endl;
    return false;
}


bool YarpMotorControlHelper::getEncoderSpeed(int j, double *sp) //NOT TESTED
{
    ARMARX_INFO << deactivateSpam(3) << " motionControl: getEncoderSpeed " << std::endl;
    return getRefSpeed(j, sp);
}

bool YarpMotorControlHelper::getEncoderSpeeds(double *spds) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getEncoderSpeeds " << std::endl;
    return getRefSpeeds(spds);
}

bool YarpMotorControlHelper::getEncoderAcceleration(int j, double *spds) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getEncoderAcceleration " << std::endl;
    return false;
}

bool YarpMotorControlHelper::getEncoderAccelerations(double *accs) //NOT IMPLEMENTED
{
    ARMARX_INFO << deactivateSpam() << " motionControl: getEncoderAccelerations " << std::endl;
    return false;
}


