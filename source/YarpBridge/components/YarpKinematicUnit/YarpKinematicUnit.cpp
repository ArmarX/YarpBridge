/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpKinematicUnit
 * @author     Mirko Waechter ( mirko.waechter at kit dot edu)
 * @author     Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "YarpKinematicUnit.h"
#include "YarpMotorInterfaceHelper.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/EndEffector/EndEffector.h>

using namespace armarx;
using namespace VirtualRobot;


void YarpKinematicUnit::onInitKinematicUnit()
{
    std::string robotName  = getProperty<std::string>("Robot").getValue();
    std::string parts  = getProperty<std::string>("Parts").getValue();
    std::vector<std::string> partPairs;
    boost::split(partPairs,
                 parts,
                 boost::is_any_of(";"),
                 boost::token_compress_on);
    for(size_t i = 0; i < partPairs.size(); i++)
    {
        std::vector<std::string> ids;
        boost::split(ids,
                     partPairs.at(i),
                     boost::is_any_of(":"),
                     boost::token_compress_on);
        if(ids.size() != 2)
        {
            ARMARX_WARNING << "wrong size of identifiers: " << ids.size() << " at substring " << i << " : " << partPairs.at(i);
        }
        else
        {
            yarp::os::Property options;
            options.put("robot", robotName.c_str());
            options.put("part", ids[0]);
            yarpMotorInterfaces[ids[1]] = new YarpMotorInterfaceHelper(options);
            RobotNodeSetPtr rns = robot->getRobotNodeSet(ids[1]);
            std::vector<RobotNodePtr> nodes = rns->getAllRobotNodes();
            for(size_t j = 0; j < nodes.size(); j++)
            {
                nameIndexMap[nodes.at(j)->getName()] =  std::make_pair(ids[1], j);
            }            
        }
    }
}

void YarpKinematicUnit::onStartKinematicUnit()
{
    task = new PeriodicTask<YarpKinematicUnit>(this, &YarpKinematicUnit::report, getProperty<int>("ReportCycleTime").getValue());
    bool success = true;
    for(InterfaceMap::iterator it = yarpMotorInterfaces.begin(); it != yarpMotorInterfaces.end(); it++)
    {
        success &= it->second->open();
    }



    if(success)
        task->start();

}

void YarpKinematicUnit::onExitKinematicUnit()
{
    if(task)
        task->stop();
    for(InterfaceMap::iterator it = yarpMotorInterfaces.begin(); it != yarpMotorInterfaces.end(); it++)
    {
        it->second->close();        
        delete it->second;

    }
    yarpMotorInterfaces.clear();
}


PropertyDefinitionsPtr YarpKinematicUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new YarpKinematicUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}





void armarx::YarpKinematicUnit::setJointAngles(const NameValueMap &jointsMap , const Ice::Current &)
{

    //VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("Left Arm");
    //std::vector<VirtualRobot::RobotNodePtr> nodes = rns->getAllRobotNodes();

    for(NameValueMap::const_iterator it = jointsMap.begin(); it != jointsMap.end(); it++)
    {
        std::string setName = nameIndexMap[it->first].first;
        int index = nameIndexMap[it->first].second;
        InterfaceMap::iterator itInterface = yarpMotorInterfaces.find(setName);
        if(itInterface != yarpMotorInterfaces.end())
        {
            if(!itInterface->second->getPositionInterface())
                return;
            ARMARX_VERBOSE << setName << " i: " << index << " pos: " << it->second*180/M_PI;
            if(!itInterface->second->getPositionInterface()->positionMove(index, it->second*180/M_PI))
                ARMARX_WARNING << "Could not set Position for " << setName << " i: " << index;
        }
    }
}

void armarx::YarpKinematicUnit::setJointVelocities(const NameValueMap &jointsMap, const Ice::Current &)
{
    ScopedLock lock(dataMutex);
//    ARMARX_INFO << VAROUT(jointsMap);
    // check if all joints of one parts are requested to be controlled in velocity mode
    std::map<std::string, bool> bAllVelocityMode;
    for(NameValueMap::const_iterator it = jointsMap.begin(); it != jointsMap.end(); it++)
    {
        std::map<std::string, ControlMode>::const_iterator itModes = controlModes.find(it->first);
        if (itModes == controlModes.end())
            continue;
//        ARMARX_INFO << it->first << ": " << it->second << " set name: " << nameIndexMap[it->first].first;
        auto nodeSetName = nameIndexMap[it->first].first;
        auto itPartsVelBool = bAllVelocityMode.find(nodeSetName);
        if(itPartsVelBool == bAllVelocityMode.end())
            bAllVelocityMode[nodeSetName] = (itModes->second == eVelocityControl);
        else
            itPartsVelBool->second &= (itModes->second == eVelocityControl);
    }
    for(auto &nodeSet : bAllVelocityMode)
    {
        if(nodeSet.first.empty())
            continue;
//        ARMARX_INFO << "Setting velos for " << nodeSet.first;
        if (nodeSet.second)
        {
            std::string setName = nodeSet.first;
//            ARMARX_INFO << "All velos at once for Robot Part name: " << setName;
            for(auto elem : nameIndexMap)
            {
//                ARMARX_INFO_S << "jN: " << elem.first << " nodeset: " << elem.second.first  << " jointIndex: " << elem.second.second;
            }
            InterfaceMap::iterator itInterface = yarpMotorInterfaces.find(setName);
            if(itInterface == yarpMotorInterfaces.end())
            {
                ARMARX_WARNING << "Cannot find the proper yarpMotorInterface!";
                return;
            }

            int axes;
            if(!itInterface->second->getVelocityInterface()->getAxes(&axes)) {
                ARMARX_WARNING << "Cannot Get the number of controlled axes";
                return;
            }

            double* values = new double[axes];
            for(int i=0; i<axes; i++)
                values[i] = 0.0;

            for(NameValueMap::const_iterator it = jointsMap.begin(); it != jointsMap.end(); it++)
            {
                if(nameIndexMap[it->first].first != setName)
                    continue;
                int index = nameIndexMap[it->first].second;
                values[index] = it->second * 180.0f/M_PI;
//                ARMARX_INFO_S << "loop jN: " << it->first << " nodeset: " << setName  << " jointIndex: " << index << VAROUT(values[index]);
            }

            if(!itInterface->second->getVelocityInterface()->velocityMove(values))
                ARMARX_WARNING << "Could not set batch velocity for part " << setName;

//            for(int i=0; i<axes; i++)
//            {
//                ARMARX_INFO << "Joint " << i << ": " << values[i];
//            }

            delete[] values;

        }
        else
        {
//            ARMARX_INFO << "velos/refspeed seperatly for Robot Part name: " << nodeSet.first;
            // not all joints are requested for the velocity mode
            for(NameValueMap::const_iterator it = jointsMap.begin(); it != jointsMap.end(); it++)
            {
                std::map<std::string, ControlMode>::const_iterator itModes = controlModes.find(it->first);
                if(itModes == controlModes.end() || nameIndexMap[itModes->first].first !=  nodeSet.first)
                    continue;
                ControlMode mode = itModes->second;
                switch (mode)
                {
                case ePositionControl:
                {
                    std::string setName = nodeSet.first;
                    int index = nameIndexMap[it->first].second;
                    InterfaceMap::iterator itInterface = yarpMotorInterfaces.find(setName);
                    if(itInterface != yarpMotorInterfaces.end())
                    {
                        if(!itInterface->second->getPositionInterface())
                            return;
//                        ARMARX_VERBOSE << setName << " i: " << index << "vel: " << it->second * 180.0f/M_PI;
                        itInterface->second->getPositionInterface()->setRefSpeed(index, fabs(it->second * 180.0f/M_PI));
                    }
                    break;
                }

                case eVelocityControl:
                {
                    std::string setName = nodeSet.first;
                    int index = nameIndexMap[it->first].second;
                    InterfaceMap::iterator itInterface = yarpMotorInterfaces.find(setName);
                    if(itInterface != yarpMotorInterfaces.end())
                    {
                        if(!itInterface->second->getVelocityInterface())
                        {
                            ARMARX_WARNING << "velocity interface not possible";
                            return;
                        }
//                        ARMARX_VERBOSE << "VelMode" << setName << " i: " << index << "vel: " << it->second * 180.0f/M_PI;
                        if(!itInterface->second->getVelocityInterface()->velocityMove(index, it->second * 180.0f/M_PI))
                            ARMARX_WARNING << "Could not set joint velocity of " << it->first;
                    }
                    break;
                }
                default:

                    break;


                }
            }
        }
    }
}

void armarx::YarpKinematicUnit::setJointTorques(const NameValueMap &, const Ice::Current &)
{

}

void armarx::YarpKinematicUnit::setJointAccelerations(const NameValueMap &, const Ice::Current &)
{
}

void armarx::YarpKinematicUnit::setJointDecelerations(const NameValueMap &, const Ice::Current &)
{
}

void YarpKinematicUnit::report()
{
    NameValueMap newValues;

    for(InterfaceMap::iterator it = yarpMotorInterfaces.begin(); it != yarpMotorInterfaces.end(); it++)
    {
        VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(it->first);
        std::vector<VirtualRobot::RobotNodePtr> nodes = rns->getAllRobotNodes();
        YarpMotorInterfaceHelper* interface = it->second;

        int jointsNum = 0;
        if(!interface->getEncoderInterface()->getAxes(&jointsNum))
        {
            ARMARX_WARNING << deactivateSpam(3) << "Failed to get joint count of "<<it->first;
            return;
        }

        double* encoderValues = new double[jointsNum];
        if(!interface->getEncoderInterface()->getEncoders(encoderValues))
        {                
            ARMARX_WARNING << deactivateSpam(3) << "Failed to get the encoder values of " << it->first;
            delete [] encoderValues;
            return;
        }
        
        //ARMARX_IMPORTANT <<"Joint set "<<it->first;        
        for(size_t i = 0; i < nodes.size() && i < (size_t)jointsNum; i++)
        {
            //ARMARX_IMPORTANT <<"\t"<<nodes.at(i)->getName() << " : " <<encoderValues[i];
            newValues[nodes.at(i)->getName()] = encoderValues[i]/180.0*M_PI;
        }
        delete [] encoderValues;
        encoderValues = NULL;
    }
    Ice::Long timestamp = IceUtil::Time::now().toMicroSeconds();
    listenerPrx->reportJointAngles(newValues, timestamp, true);

    reportJointVelocities();



}

void YarpKinematicUnit::reportJointVelocities()
{
    NameValueMap newValues;

    for(InterfaceMap::iterator it = yarpMotorInterfaces.begin(); it != yarpMotorInterfaces.end(); it++)
    {
        VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(it->first);
        std::vector<VirtualRobot::RobotNodePtr> nodes = rns->getAllRobotNodes();
        YarpMotorInterfaceHelper* interface = it->second;

        int jointsNum = 0;
        if(!interface->getEncoderInterface()->getAxes(&jointsNum))
        {
            ARMARX_WARNING << deactivateSpam(3) << "Failed to get joint count of "<<it->first;
            return;
        }

        double* encoderValues = new double[jointsNum];
        if(!interface->getEncoderInterface()->getEncoderSpeeds(encoderValues))
        {
            ARMARX_WARNING << deactivateSpam(3) << "Failed to get the encoder values of " << it->first;
            delete [] encoderValues;
            return;
        }

        //ARMARX_IMPORTANT <<"Joint set "<<it->first;
        for(size_t i = 0; i < nodes.size() && i < (size_t)jointsNum; i++)
        {
            //ARMARX_IMPORTANT <<"\t"<<nodes.at(i)->getName() << " : " <<encoderValues[i];
            newValues[nodes.at(i)->getName()] = encoderValues[i]/180.0*M_PI;
        }
        delete [] encoderValues;
        encoderValues = NULL;
    }
    Ice::Long timestamp = IceUtil::Time::now().toMicroSeconds();
    listenerPrx->reportJointVelocities(newValues, timestamp, true);
}

void YarpKinematicUnit::stop(const Ice::Current &)
{

    for(auto interface : yarpMotorInterfaces)
    {
        YarpMotorInterfaceHelper* i = interface.second;
        i->getVelocityInterface()->stop();
    }
}


void armarx::YarpKinematicUnit::requestJoints(const Ice::StringSeq &, const Ice::Current &)
{
}

void armarx::YarpKinematicUnit::releaseJoints(const Ice::StringSeq &, const Ice::Current &)
{
}

void YarpKinematicUnit::switchControlMode(const NameControlModeMap &controlModes, const Ice::Current &)
{
    ScopedLock lock(dataMutex);
//    ARMARX_INFO << "new modes: " << controlModes;
    for(auto& mode : controlModes)
    {

        std::string setName = nameIndexMap[mode.first].first;
        int index = nameIndexMap[mode.first].second;
        InterfaceMap::iterator itInterface = yarpMotorInterfaces.find(setName);
        if(itInterface != yarpMotorInterfaces.end())
        {
            switch(mode.second)
            {
            case ePositionControl:
                if(!itInterface->second->getControlModeInterface())
                {
                    ARMARX_WARNING << "Could not get ControlMode interface for " << setName << " i: " << index;
                    return;
                }
                if(!itInterface->second->getControlModeInterface()->setPositionMode(index))
                    ARMARX_WARNING << "Could not set Position mode for " << setName << " i: " << index;
                break;
            case eVelocityControl:
                if(!itInterface->second->getControlModeInterface())
                {
                    ARMARX_WARNING << "Could not get ControlMode interface for " << setName << " i: " << index;
                    return;
                }
                if(!itInterface->second->getControlModeInterface()->setVelocityMode(index))
                    ARMARX_WARNING << "Could not set Velocity mode for " << setName << " i: " << index;
                break;
            default:
                ARMARX_WARNING << deactivateSpam(1) << "ControlMode " << mode.second << " is not supported";


            }
        }

        this->controlModes[mode.first] = mode.second;
    }
//    ARMARX_INFO << "new modes: " << controlModes;
}


NameControlModeMap YarpKinematicUnit::getControlModes(const Ice::Current &c)
{
        return controlModes;
}

