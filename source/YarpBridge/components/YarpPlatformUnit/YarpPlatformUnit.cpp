/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpFTInterfaceHelper
 * @author     Luca Muratore ( luca dot muratore dot iit dot it)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "YarpPlatformUnit.h"

#include <yarp/os/Network.h>
#include <boost/lexical_cast.hpp>


using namespace armarx;





void armarx::YarpPlatformUnit::move(Ice::Float x, Ice::Float y, Ice::Float alpha, const Ice::Current &)
{
    boost::mutex::scoped_lock lock(mutex);

    // NOTE hardcoded command to move the robot, hardcoded step size on x and y, hardcoded interpolation

    // create the bottle with the requested command
    yarp::os::Bottle cmd_bottle;
    yarp::os::Bottle& cmd_param_list = cmd_bottle.addList();
    cmd_param_list.addString("go_there");
    cmd_param_list.addDouble(x);
    cmd_param_list.addDouble(y);
    cmd_param_list.addDouble(0.0);
    cmd_param_list.addDouble(0.0);
    cmd_param_list.addDouble(0.0);
    cmd_param_list.addDouble(0.0);
    cmd_param_list.addDouble(0.0);
    cmd_param_list.addDouble(0.1);
    cmd_param_list.addDouble(0.1);
    cmd_param_list.addString("linear");

    yarp::os::Bottle& b=command_out.prepare();
    b.clear();
    b.append(cmd_bottle);
    b.addInt(3); // NOTE hardcoded seq num

    ARMARX_INFO << "bottle: " << b.toString();

    // send it
    command_out.write();


    listenerPrx->reportNewTargetPose(x,y,alpha);
}

void armarx::YarpPlatformUnit::moveRelative(Ice::Float x, Ice::Float y, Ice::Float alpha, Ice::Float positionalAccuracy, Ice::Float rotationalAccuracy, const Ice::Current &)
{
    ARMARX_WARNING << deactivateSpam(10) << "not yet implemented";

    move(x, y, alpha);

}

void armarx::YarpPlatformUnit::setMaxVelocities(Ice::Float, Ice::Float, const Ice::Current &)
{
    ARMARX_WARNING << deactivateSpam(10) << "not yet implemented";
}

void armarx::YarpPlatformUnit::onInitPlatformUnit()
{

    // get the properties
    std::string moduleName = getProperty<std::string>("Module").getValue();
    std::string commandPortName = getProperty<std::string>("CommandPortName").getValue();
    std::string unitName = getProperty<std::string>("UnitName").getValue();;
    // define the port names
    std::string localPort = std::string("/" + unitName + "/" + moduleName);
    std::string remotePort = std::string("/" + moduleName + "/" + commandPortName);
    // open the YARP local command port

    ARMARX_INFO << "opening local port" << localPort;
    if( !command_out.open(localPort)) {
        ARMARX_ERROR << "Error while opening the local YARP port " << localPort;
    }

    ARMARX_INFO << "connecting to remote port" << remotePort;

    // connect the YARP local command port to the YARP remote command port
    if( !yarp::os::Network::connect(localPort, remotePort)) {
         ARMARX_ERROR << "Error while connecting the local YARP port " << localPort << " to the remote YARP port " << remotePort;
    }

    reportTask = new PeriodicTask<YarpPlatformUnit>(this, &YarpPlatformUnit::report, getProperty<int>("ReportCycleTime").getValue());

}

void armarx::YarpPlatformUnit::onStartPlatformUnit()
{
    reportTask->start();
}

void armarx::YarpPlatformUnit::onExitPlatformUnit()
{
    // close the local YARP port
    command_out.close();

    reportTask->stop();
}


PropertyDefinitionsPtr YarpPlatformUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new YarpPlatformUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}

void YarpPlatformUnit::report()
{
    // todo implement me
    //listenerPrx->reportPlatformPose(0,0,0);
    //listenerPrx->reportPlatformVelocity(0,0,0);
}
