/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpFTInterfaceHelper
 * @author     Luca Muratore ( luca dot muratore dot iit dot it)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpPlatformUnit_H
#define _ARMARX_COMPONENT_YarpBridge_YarpPlatformUnit_H


#include <RobotAPI/components/units/PlatformUnit.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <yarp/os/BufferedPort.h>
#include <yarp/os/Bottle.h>

namespace armarx
{
    /**
     * @class YarpPlatformUnitPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class YarpPlatformUnitPropertyDefinitions:
        public PlatformUnitPropertyDefinitions
    {
    public:
        YarpPlatformUnitPropertyDefinitions(std::string prefix):
            PlatformUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Module", std::string("walking"), "Module name that moves the robot");
            defineOptionalProperty<std::string>("CommandPortName", std::string("command:i"), "YARP module command port name");
            defineOptionalProperty<std::string>("UnitName", std::string("walkingPlatformUnit"), "Platform unit name");
            defineOptionalProperty<int>("ReportCycleTime", 10, "CycleTime in ms of the sensor value reporting thread. Doesnot affect cycle time of walkman.");
        }
    };

    /**
     * @class YarpPlatformUnit
     * @brief A brief description
     *
     * Detailed Description
     */
    class YarpPlatformUnit :
        virtual public armarx::Component,
            virtual public PlatformUnit
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "YarpPlatformUnit";
        }
    public:
        void move(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &c = ::Ice::Current());
        void moveRelative(Ice::Float, Ice::Float, Ice::Float, Ice::Float, Ice::Float, const Ice::Current &c = ::Ice::Current());
        void setMaxVelocities(Ice::Float, Ice::Float, const Ice::Current &c = ::Ice::Current());

        // PlatformUnit interface
        void onInitPlatformUnit();
        void onStartPlatformUnit();
        void onExitPlatformUnit();
    protected:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        void report();

        // PlatformUnitInterface interface

        PeriodicTask<YarpPlatformUnit>::pointer_type reportTask;
    private:
        yarp::os::BufferedPort<yarp::os::Bottle> command_out;

        boost::mutex mutex;
    };
}

#endif
