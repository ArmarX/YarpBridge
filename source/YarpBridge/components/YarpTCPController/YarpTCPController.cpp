/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpTCPController
 * @author     David Schiebener ( david dot schiebener at kit dot edu)
 * @author     Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "YarpTCPController.h"

using namespace armarx;

#include <yarp/os/Property.h>
#include <yarp/sig/Vector.h>

using namespace yarp::os;
using namespace yarp::dev;
using namespace yarp::sig;

#define MAX_TORSO_PITCH         30.0

void YarpTCPController::onInitComponent()
{
}


void YarpTCPController::onConnectComponent()
{
    ARMARX_VERBOSE << "1a";
    std::string robotName  = getProperty<std::string>("Robot").getValue();
    std::string partName  = getProperty<std::string>("YarpRobotPart").getValue();
    std::string objName = getProperty<std::string>("ObjectName").getValue();
    useVergence = getProperty<bool>("UseVergence").getValue();
    blockNeckRoll = getProperty<bool>("BlockNeckRoll").getValue();

    ARMARX_VERBOSE << "1b";

    // open a client interface to connect to the gaze server
    yarp::os::Property optGaze("(device gazecontrollerclient)");
    optGaze.put("remote","/iKinGazeCtrl");
    optGaze.put("local",("/"+objName+"/gaze").c_str());

    ARMARX_VERBOSE << "1c";

    if (!drvGaze.open(optGaze))
    {
        ARMARX_ERROR << "Cannot open gaze controller device"; 
        return;
    }

    ARMARX_VERBOSE << "1d";

    // open the view
    if(!drvGaze.view(igaze))
    {
        ARMARX_ERROR << "Cannot view iKinGazeCtrl interface"; 
        return;
    }

    ARMARX_VERBOSE << "1e";

    // latch the controller context in order to preserve
    // it after closing the module
    // the context contains the tracking mode, the neck limits and so on.
    igaze->storeContext(&startup_context_gaze);

    ARMARX_VERBOSE << "2";

    // set trajectory time:
    igaze->setNeckTrajTime(2.5);
    igaze->setEyesTrajTime(2.5);


    //igaze->setSaccadesStatus(false);
    if(useVergence)
        igaze->clearEyes();
    else
    {
        igaze->blockEyes(0);
    }

    if(blockNeckRoll)
        igaze->blockNeckRoll();

    // open a client interface to connect to the arm cartesian control server
    yarp::os::Property optArm("(device cartesiancontrollerclient)");
    optArm.put("remote",("/" + robotName + "/cartesianController/" + partName).c_str());
    optArm.put("local",("/"+objName+"/cartesian/"+partName).c_str());

    if (!drvArm.open(optArm))
    {
        ARMARX_ERROR << "Cannot open cartesian controller device"; 
        return;
    }

    // open the view
    if(!drvArm.view(iarm))
    {
        ARMARX_ERROR << "Cannot view ICartesianControl interface"; 
        return;
    }

    ARMARX_VERBOSE << "3";

    // latch the controller context in order to preserve
    // it after closing the module
    iarm->storeContext(&startup_context_arm);

    // get the torso dofs
    Vector newDof, curDof;
    iarm->getDOF(curDof);
    newDof = curDof;

    // enable the torso yaw and pitch
    // disable the torso roll
    int dof = getProperty<int>("DOF").getValue();
    switch(dof) {
    case 8:
        newDof[0]=0;
        newDof[1]=0;
        newDof[2]=1;
        break;

    case 9:
        newDof[0]=1;
        newDof[1]=0;
        newDof[2]=1;
        break;

    case 10: 
        newDof[0]=1;
        newDof[1]=1;
        newDof[2]=1;
        break;

     default:     //disable torso
        newDof[0]=0;
        newDof[1]=0;
        newDof[2]=0;
    };

    // impose some restriction on the torso pitch
    int axis = 0; // pitch joint
    double min, max;
    // we keep the lower limit
    iarm->getLimits(axis, &min, &max);
    iarm->setLimits(axis, min, MAX_TORSO_PITCH);
    iarm->setDOF(newDof, curDof);

    ARMARX_VERBOSE << "4";

    // testing 
    //yarp::sig::Vector pos(3);
    //yarp::sig::Vector orient(4);
    //iarm->getPose(pos, orient);
    //ARMARX_INFO<<"Current Position: x:"<<pos[0]<<", y:"<<pos[1]<<", z:"<<pos[2];
}


void YarpTCPController::onExitComponent()
{
    if(igaze)
    {
        igaze->stopControl();   
        igaze->restoreContext(startup_context_gaze);
    }        
    
    if(iarm)
    {
        iarm->stopControl();
        iarm->restoreContext(startup_context_arm);
    } 

    drvGaze.close();
    drvArm.close();
}


void YarpTCPController::setTCPTarget(const std::string &nodeSetName, const std::string &tcpName, const::armarx::FramedPositionBasePtr &targetPosition, const::armarx::FramedOrientationBasePtr &targetOrientation, const int timeForMotionInMS, const Ice::Current &c)
{
    double time = timeForMotionInMS / 1000.0;
    yarp::sig::Vector pos(3);
    pos[0] = targetPosition->x / 1000.0;
    pos[1] = targetPosition->y / 1000.0;
    pos[2] = targetPosition->z / 1000.0;

    bool result = false;

    if (tcpName.compare("Right Arm TCP") == 0)
    {
        if (targetOrientation)
        {
            yarp::sig::Vector orient(4);
            if (targetOrientation->qw==1)
            {
                orient[0] = 1;
                orient[1] = orient[2] = orient[3] = 0;
            }
            else
            {
                orient[0] = targetOrientation->qx / sqrt(1-targetOrientation->qw*targetOrientation->qw);
                orient[1] = targetOrientation->qy / sqrt(1-targetOrientation->qw*targetOrientation->qw);
                orient[2] = targetOrientation->qz / sqrt(1-targetOrientation->qw*targetOrientation->qw);
                orient[3] = 2*acos(targetOrientation->qw);
            }
            result = iarm->goToPose(pos, orient, time);
        }
        else
        {
            result = iarm->goToPosition(pos, time);
        }
        ARMARX_VERBOSE << "Cartesian controller returned " << result;
    }
    else if (tcpName.compare("Left Arm TCP") == 0)
    {
        // todo
    }
    else if (tcpName.compare("VirtualCentralGaze") == 0)
    {
        if(!useVergence)
        {
            igaze->blockEyes();
            //igaze->setSaccadesStatus(false);
        } 
        igaze->lookAtFixationPoint(pos);
    }

}


void YarpTCPController::stopTCP(const std::string &nodeSetName, const std::string &tcpName, const Ice::Current &c)
{
    if (tcpName.compare("Right Arm TCP") == 0)
    {
        iarm->stopControl();
    }
    else if (tcpName.compare("Left Arm TCP") == 0)
    {
        // todo
    }
    else if (tcpName.compare("VirtualCentralGaze") == 0)
    {
        igaze->stopControl();
    }
}


PropertyDefinitionsPtr YarpTCPController::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new YarpTCPControllerPropertyDefinitions(getConfigIdentifier()));
}



