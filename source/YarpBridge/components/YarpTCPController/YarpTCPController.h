/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpTCPController
 * @author     David Schiebener ( david dot schiebener at kit dot edu)
 * @author     Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpTCPController_H
#define _ARMARX_COMPONENT_YarpBridge_YarpTCPController_H


#include <YarpBridge/interface/TCPPoseControlUnit.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// YARP related header files
#include <yarp/dev/Drivers.h>
#include <yarp/dev/CartesianControl.h>
#include <yarp/dev/GazeControl.h>
#include <yarp/dev/PolyDriver.h>


namespace armarx
{
    /**
     * @class YarpTCPControllerPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class YarpTCPControllerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        YarpTCPControllerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Robot", std::string("icub"), "Robot name");
            defineOptionalProperty<std::string>("YarpRobotPart", "right_arm");
            defineOptionalProperty<int>("DOF", 9, "iCub controller degree of freedom. 8:yaw, 9:yaw/pitch, 10:yaw/roll/pitch");
            defineOptionalProperty<bool>("UseVergence", false, "Control vergence of the eyes for gazing");
            defineOptionalProperty<bool>("BlockNeckRoll", true, "Block the neck roll");
        }
    };

    /**
     * @class YarpTCPController
     * @brief A brief description
     *
     * Detailed Description
     */
    class YarpTCPController :
        virtual public armarx::Component,
        virtual public TCPPoseControlUnitInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "YarpTCPController";
        }

        void setTCPTarget(const std::string &nodeSetName, const std::string &tcpName, const::armarx::FramedPositionBasePtr &targetPosition, const::armarx::FramedOrientationBasePtr &targetOrientation, const int timeForMotionInMS, const Ice::Current &c = Ice::Current());
        void stopTCP(const std::string &nodeSetName, const std::string &tcpName, const Ice::Current &c = Ice::Current());

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        // Component
    public:
        virtual void onInitComponent() override;
        virtual void onConnectComponent() override;
        virtual void onExitComponent() override;

        // yarp related properties 
    private:
        yarp::dev::PolyDriver drvArm;
        yarp::dev::PolyDriver drvGaze;
        yarp::dev::ICartesianControl *iarm;
        yarp::dev::IGazeControl      *igaze;
        int startup_context_gaze;
        int startup_context_arm;
        bool useVergence;
        bool blockNeckRoll;
    };
}

#endif
