
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore YarpPrimitiveProvider)
 
armarx_add_test(YarpPrimitiveProviderTest YarpPrimitiveProviderTest.cpp "${LIBS}")