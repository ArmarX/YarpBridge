/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpPrimitiveProvider
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpPrimitiveProvider_H
#define _ARMARX_COMPONENT_YarpBridge_YarpPrimitiveProvider_H


#include <ArmarXCore/core/Component.h>

#include <VisionX/interface/components/PointCloudSegmenter.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/memory/SegmentedMemory.h>
#include <MemoryX/libraries/memorytypes/segment/EnvironmentalPrimitiveSegment.h>

#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <yarp/os/Bottle.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/ContactStyle.h>
#include <yarp/os/Network.h>

namespace armarx
{
    /**
     * @class YarpPrimitiveProviderPropertyDefinitions
     * @brief
     */
    class YarpPrimitiveProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        YarpPrimitiveProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of WorkingMemory component");
            defineOptionalProperty<std::string>("PointCloudSegmentationTopicName", "SegmentedPointCloud", "Name of the topic for point cloud segmentations");
        }
    };

    /**
     * @defgroup Component-YarpPrimitiveProvider YarpPrimitiveProvider
     * @ingroup YarpBridge-Components
     * A description of the component YarpPrimitiveProvider.
     * 
     * @class YarpPrimitiveProvider
     * @ingroup Component-YarpPrimitiveProvider
     * @brief Brief description of class YarpPrimitiveProvider.
     * 
     * Detailed description of class YarpPrimitiveProvider.
     */
    class YarpPrimitiveProvider :
        virtual public armarx::Component,
        virtual public visionx::PointCloudSegmentationListener
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "YarpPrimitiveProvider";
        }

        virtual void reportNewPointCloudSegmentation(const ::Ice::Current& = ::Ice::Current());

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        memoryx::EnvironmentalPrimitiveSegmentBasePrx environmentalPrimitiveSegment;
        yarp::os::BufferedPort<yarp::os::Bottle> outputPort;
    };
}

#endif
