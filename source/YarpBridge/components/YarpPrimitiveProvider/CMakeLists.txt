armarx_component_set_name("YarpPrimitiveProvider")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

# For AffordanceExtractionLuib
add_definitions(-DYARP_AVAILABLE)

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MemoryXMemoryTypes VisionXInterfaces AffordanceExtractionLib ${YARP_LIBRARIES} ${Simox_LIBRARIES})

set(SOURCES
./YarpPrimitiveProvider.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)
set(HEADERS
./YarpPrimitiveProvider.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
