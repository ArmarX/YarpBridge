/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpPrimitiveProvider
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "YarpPrimitiveProvider.h"

#include <VisionX/libraries/AffordanceExtraction/PrimitiveSet.h>

using namespace armarx;

void YarpPrimitiveProvider::onInitComponent()
{
    std::string port_name_out="/yarp_armarx/primitives:o";
    std::string port_name_in="/yarp_armarx/primitives:i";

    outputPort.open(port_name_out);

    if (!yarp::os::NetworkBase::isConnected(port_name_out, port_name_in))
    {
        yarp::os::ContactStyle style;
        style.persistent = true;

        ARMARX_INFO << "Connecting " << port_name_out << " to " << port_name_in;
        yarp::os::Network::connect(port_name_out, port_name_in, style);
    }

    usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
    usingTopic(getProperty<std::string>("PointCloudSegmentationTopicName").getValue());
}


void YarpPrimitiveProvider::onConnectComponent()
{
    workingMemoryPrx = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
    if(!workingMemoryPrx)
    {
        ARMARX_ERROR << "Failed to obtain working memory proxy";
        return;
    }

    environmentalPrimitiveSegment = workingMemoryPrx->getEnvironmentalPrimitiveSegment();
    if(!environmentalPrimitiveSegment)
    {
        ARMARX_ERROR << "Failed to obtain environmental primitive segment pointer";
        return;
    }
}

void YarpPrimitiveProvider::reportNewPointCloudSegmentation(const Ice::Current &)
{
    AffordanceExtractionLib::PrimitiveSetPtr primitives(new AffordanceExtractionLib::PrimitiveSet(environmentalPrimitiveSegment));

    yarp::os::Bottle& toWrite = outputPort.prepare();
    toWrite.clear();
    toWrite.append(primitives->toYarpBottle());
    outputPort.write();
}

void YarpPrimitiveProvider::onDisconnectComponent()
{
}

void YarpPrimitiveProvider::onExitComponent()
{
}

armarx::PropertyDefinitionsPtr YarpPrimitiveProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new YarpPrimitiveProviderPropertyDefinitions(
                                      getConfigIdentifier()));
}

