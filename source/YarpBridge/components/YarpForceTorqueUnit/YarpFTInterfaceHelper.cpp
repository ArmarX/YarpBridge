/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpFTInterfaceHelper
 * @author     Luca Muratore ( luca dot muratore at iit dot it) based on the work of Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "YarpFTInterfaceHelper.h"
#include <ArmarXCore/core/logging/Logging.h>

using namespace std;
using namespace armarx;
using namespace yarp::dev;
using namespace yarp::os;

// YARP_DECLARE_DEVICES(icubmod)
// YARP_REGISTER_DEVICES(icubmod)

YarpFTInterfaceHelper::YarpFTInterfaceHelper(const yarp::os::Property& options)
{

   iSensor = NULL;

   YarpFTInterfaceHelper::options = options;
}

YarpFTInterfaceHelper::~YarpFTInterfaceHelper()
{
    YarpFTInterfaceHelper::close();
}

void YarpFTInterfaceHelper::close()
{
    driver.close();
}

bool YarpFTInterfaceHelper::open()
{
    string robotName = options.find("robot").asString();
    string partName = options.find("part").asString();
    string unitName = (options.check("unit_name")) ? options.find("unit_name").asString() : "YarpForceTorqueUnit";

    string remoteName = string("/") + robotName + string("/") + partName + string("/analog:o/forceTorque");
    string localName = string("/") + unitName + string("/") + robotName + string("/") + partName;

    // put the required options
    options.put("device", "analogsensorclient");
    options.put("local", localName.c_str());
    options.put("remote", remoteName.c_str());
    // TBD check if "rate" is needed

    // open the driver
    if (!driver.open(options)) {
        ARMARX_ERROR_S <<"Cannot open yarp device driver! options: " << options.toString();
        return false;
    }

    // view the interfaces
    driver.view(iSensor);
    if(!iSensor) {
        ARMARX_ERROR_S << "Failed to open AnalogSensor interface of " << partName;
        return false;
    }

    return true;
}



