/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpFTInterfaceHelper
 * @author     Luca Muratore ( luca dot muratore dot iit dot it) based on the work of Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpFTInterfaceHelper_H
#define _ARMARX_COMPONENT_YarpBridge_YarpFTInterfaceHelper_H


#include <string>

// yarp os headers
#include <yarp/os/Property.h>

// yarp interfaces
#include <yarp/dev/PolyDriver.h>
#include <yarp/dev/IAnalogSensor.h>


namespace armarx
{

    /**
     * @class YarpFTInterfaceHelper
     * @brief F-T sensor YARP interface helper
     *
     */
    class YarpFTInterfaceHelper
    {
    public:
        YarpFTInterfaceHelper(const yarp::os::Property& properties);
        virtual ~YarpFTInterfaceHelper();

        bool open();
        void close();

        yarp::dev::IAnalogSensor* getAnalogSensor() {
            return iSensor;
        }

    private:
        yarp::os::Property options;
        yarp::dev::PolyDriver   driver;
        yarp::dev::IAnalogSensor    *iSensor;
    };
}

#endif
