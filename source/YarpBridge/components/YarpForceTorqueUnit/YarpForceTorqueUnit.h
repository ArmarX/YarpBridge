/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpForceTorqueUnit
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpForceTorqueUnit_H
#define _ARMARX_COMPONENT_YarpBridge_YarpForceTorqueUnit_H


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/components/units/ForceTorqueUnit.h>

#include "YarpFTInterfaceHelper.h"

namespace armarx
{
    /**
     * @class YarpForceTorqueUnitPropertyDefinitions
     * @brief
     */
    class YarpForceTorqueUnitPropertyDefinitions:
        public armarx::ForceTorqueUnitPropertyDefinitions
    {
    public:
        YarpForceTorqueUnitPropertyDefinitions(std::string prefix):
            armarx::ForceTorqueUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Parts", "l_arm_ft:FTLeftArm;r_arm_ft:FTRightArm;l_leg_ft:FTLeftLeg;r_leg_ft:FTRighttLeg", "List of force torque sensors");
            defineOptionalProperty<std::string>("Robot", std::string("icub"), "Robot name");
            defineOptionalProperty<int>("ReportCycleTime", 10, "CycleTime in ms of the sensor value reporting thread. Doesnot affect cycle time of icub.");
        }
    };

    /**
     * @defgroup Component-YarpForceTorqueUnit YarpForceTorqueUnit
     * @ingroup YarpBridge-Components
     * A description of the component YarpForceTorqueUnit.
     * 
     * @class YarpForceTorqueUnit
     * @ingroup Component-YarpForceTorqueUnit
     * @brief Brief description of class YarpForceTorqueUnit.
     * 
     * Detailed description of class YarpForceTorqueUnit.
     */
    class YarpForceTorqueUnit :
        virtual public armarx::ForceTorqueUnit
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "YarpForceTorqueUnit";
        }

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

        // ForceTorqueUnit interface
    public:
        void onInitForceTorqueUnit();
        void onStartForceTorqueUnit();
        void onExitForceTorqueUnit();

        // ForceTorqueUnitInterface interface
    public:
        void setOffset(const FramedDirectionBasePtr &, const FramedDirectionBasePtr &, const Ice::Current &);
        void setToNull(const Ice::Current &);


    private:

        void report();

        std::string agentName;

        PeriodicTask<YarpForceTorqueUnit>::pointer_type task;

        std::map<std::string, YarpFTInterfaceHelper*>  interfaceMap;

    };
}

#endif
