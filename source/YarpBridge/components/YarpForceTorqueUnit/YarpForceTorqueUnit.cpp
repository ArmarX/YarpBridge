/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpForceTorqueUnit
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "YarpForceTorqueUnit.h"


using namespace armarx;



armarx::PropertyDefinitionsPtr YarpForceTorqueUnit::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new YarpForceTorqueUnitPropertyDefinitions(
                                              getConfigIdentifier()));
}

void YarpForceTorqueUnit::onInitForceTorqueUnit()
{
    agentName = getProperty<std::string>("AgentName").getValue();

    task = new PeriodicTask<YarpForceTorqueUnit>(this, &YarpForceTorqueUnit::report, getProperty<int>("ReportCycleTime").getValue());

    std::string robotName = getProperty<std::string>("Robot").getValue();
    std::string parts = getProperty<std::string>("Parts").getValue();
    std::vector<std::string> partPairs;

    boost::split(partPairs, parts, boost::is_any_of(";"), boost::token_compress_on);

    for (size_t i = 0; i < partPairs.size(); i++)
    {
        std::vector<std::string> ids;
        boost::split(ids, partPairs.at(i), boost::is_any_of(":"), boost::token_compress_on);
        if (ids.size() != 2)
        {
            ARMARX_WARNING << "wrong size of identifiers: " << ids.size() << " at substring " << i << " : " << partPairs.at(i);
        }
        else
        {
            yarp::os::Property options;
            options.put("robot", robotName.c_str());
            options.put("part", ids[0]);
            interfaceMap[ids[1]] = new YarpFTInterfaceHelper(options);
        }
    }
}

void YarpForceTorqueUnit::onStartForceTorqueUnit()
{
    for (auto& it : interfaceMap)
    {
        it.second->open();
    }
    task->start();
}

void YarpForceTorqueUnit::report()
{
    for (auto& it : interfaceMap)
    {
        std::string sensorNodeName = it.first;
        YarpFTInterfaceHelper* helper = it.second;

        yarp::sig::Vector values;
        helper->getAnalogSensor()->read(values);

        FramedDirectionPtr forces = new FramedDirection(values[0], values[1], values[2], sensorNodeName, agentName);
        FramedDirectionPtr direction = new FramedDirection(values[3], values[4], values[5], sensorNodeName, agentName);

        listenerPrx->reportSensorValues(sensorNodeName, direction, forces);
    }
}

void YarpForceTorqueUnit::onExitForceTorqueUnit()
{
    task->stop();

    for (auto& it : interfaceMap)
    {
        YarpFTInterfaceHelper* helper = it.second;
        helper->close();
    }
}

void YarpForceTorqueUnit::setOffset(const FramedDirectionBasePtr &, const FramedDirectionBasePtr &, const Ice::Current &)
{

}

void YarpForceTorqueUnit::setToNull(const Ice::Current &)
{

}

