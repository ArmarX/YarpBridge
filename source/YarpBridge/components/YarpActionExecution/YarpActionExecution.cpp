/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpActionExecution
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "YarpActionExecution.h"
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;

#include <yarp/sig/Matrix.h>
#include <yarp/sig/Vector.h>

#include <Eigen/Core>

void YarpActionExecution::onInitComponent()
{
    std::string port_name_out = "/generic_manipulation/pose_port:o";
    std::string port_name_in = "/generic_manipulation/pose_port:i";

    outputPort.open(port_name_out);

    if (!yarp::os::NetworkBase::isConnected(port_name_out, port_name_in))
    {
        yarp::os::ContactStyle style;
        style.persistent = true;

        ARMARX_INFO << "Connecting " << port_name_out << " to " << port_name_in;
        yarp::os::Network::connect(port_name_out, port_name_in, style);
    }
}

void YarpActionExecution::onConnectComponent()
{
    FramedPosePtr x = new FramedPose();
}

void YarpActionExecution::onDisconnectComponent()
{

}

void YarpActionExecution::onExitComponent()
{

}

std::string YarpActionExecution::getCurrentState(const Ice::Current& c)
{

    std::lock_guard<std::mutex> lock(mutex);

    return "";
}

void YarpActionExecution::resetState(const Ice::Current& c)
{

    std::lock_guard<std::mutex> lock(mutex);

    yarp::os::Bottle cmd;
    cmd.addString("reset");
    cmd.addInt(0);

    ARMARX_INFO << "bottle " << cmd.toString();
}

bool YarpActionExecution::nextState(const Ice::Current& c)
{

    std::lock_guard<std::mutex> lock(mutex);

    yarp::os::Bottle cmd;
    cmd.addString("next");
    cmd.addInt(0);

    ARMARX_INFO << "bottle " << cmd.toString();

    return true;
}

void YarpActionExecution::setHandPose(const FramedPoseBasePtr& leftHandPose, const FramedPoseBasePtr& rightHandPose, const ::Ice::Current& c)
{

    std::lock_guard<std::mutex> lock(mutex);

    if (leftHandPose)
    {
        ARMARX_INFO << "leftHandPose" << leftHandPose->output();
        ARMARX_INFO << "Sending leftHandPose EEF position: " << leftHandPose->position->x << ", " << leftHandPose->position->y << ", " << leftHandPose->position->z;
        ARMARX_INFO << "Sending leftHandPose EEF orientation: " << leftHandPose->orientation->qx << ", " << leftHandPose->orientation->qy << ", " << leftHandPose->orientation->qz << ", " << leftHandPose->orientation->qw;

        setHandPose(leftHandPose, "l_hand_pose");
    }

    if (rightHandPose)
    {
        ARMARX_INFO << "rightHandPose " << rightHandPose->output();
        ARMARX_INFO << "Sending rightHandPose EEF position: " << rightHandPose->position->x << ", " << rightHandPose->position->y << ", " << rightHandPose->position->z;
        ARMARX_INFO << "Sending rightHandPose EEF orientation: " << rightHandPose->orientation->qx << ", " << rightHandPose->orientation->qy << ", " << rightHandPose->orientation->qz << ", " << rightHandPose->orientation->qw;

        setHandPose(rightHandPose, "r_hand_pose");
    }
}

void YarpActionExecution::setHandPose(const FramedPoseBasePtr& handPose, std::string name)
{
    yarp::os::Bottle cmd;


    cmd.addDouble(handPose->position->x / 1000.0);
    cmd.addDouble(handPose->position->y / 1000.0);
    cmd.addDouble(handPose->position->z / 1000.0);


    Eigen::Quaternionf q = QuaternionPtr::dynamicCast(handPose->orientation)->toEigenQuaternion();

    Eigen::Quaterniond qd = q.cast<double>();
    qd.normalize();
    cmd.addDouble(qd.x());
    cmd.addDouble(qd.y());
    cmd.addDouble(qd.z());
    cmd.addDouble(qd.w());

    if (handPose->frame == "gaze")
    {
        cmd.addString("multisense/left_camera_optical_frame");
    }
    else
    {
        cmd.addString(handPose->frame);
    }
    cmd.addString("distal");
    cmd.addString(name);

    ARMARX_INFO << "bottle " << cmd.toString();

    yarp::os::Bottle& toWrite = outputPort.prepare();
    toWrite.clear();
    toWrite.append(cmd);
    outputPort.write();
}

armarx::PropertyDefinitionsPtr YarpActionExecution::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new YarpActionExecutionPropertyDefinitions(
                                      getConfigIdentifier()));
}
