/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpBridge::ArmarXObjects::YarpActionExecution
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpBridge_YarpActionExecution_H
#define _ARMARX_COMPONENT_YarpBridge_YarpActionExecution_H


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

#include <WalkMan/interface/ActionExecutionInterface.h>

#include <yarp/os/Bottle.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/ContactStyle.h>
#include <yarp/os/Network.h>

#include <mutex>

namespace armarx
{
    /**
     * @class YarpActionExecutionPropertyDefinitions
     * @brief
     */
    class YarpActionExecutionPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        YarpActionExecutionPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-YarpActionExecution YarpActionExecution
     * @ingroup YarpBridge-Components
     * A description of the component YarpActionExecution.
     * 
     * @class YarpActionExecution
     * @ingroup Component-YarpActionExecution
     * @brief Brief description of class YarpActionExecution.
     * 
     * Detailed description of class YarpActionExecution.
     */
    class YarpActionExecution :
        virtual public armarx::Component,
        virtual public armarx::ActionExecutionInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "YarpActionExecution";
        }

        void setHandPose(const FramedPoseBasePtr& leftHandPose, const FramedPoseBasePtr& rightHandPose, const Ice::Current& c = ::Ice::Current());

        std::string getCurrentState(const Ice::Current& c = ::Ice::Current());

        void resetState(const Ice::Current& c = ::Ice::Current());

        bool nextState(const Ice::Current& c = ::Ice::Current());

        virtual void sendCommand(const std::string& cmd, const Ice::Current &) override
        {
            throw NotImplementedYetException{"Command " + cmd};
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        void setHandPose(const FramedPoseBasePtr& pose, std::string name);

        yarp::os::BufferedPort<yarp::os::Bottle> outputPort;

        std::mutex mutex;
    };
}

#endif
