/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpArmarxBridge::ArmarXObjects::ArmarxGazeControl
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarxGazeControl.h"
#include "YarpGazeControlHelper.h"
#include <yarp/os/ResourceFinder.h>
#include <yarp/dev/PolyDriverList.h>

#include <sstream>

// VirtualRobot
#include <VirtualRobot/XML/RobotIO.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

using namespace std;
using namespace armarx;
using namespace yarp::os;
using namespace yarp::dev;


PropertyDefinitionsPtr ArmarxGazeControl::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ArmarxGazeControlPropertyDefinitions(
                                      getConfigIdentifier()));
}

void ArmarxGazeControl::onInitComponent()
{
    robotName  = getProperty<std::string>("Robot").getValue();

    usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
    //usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
}


void ArmarxGazeControl::onConnectComponent()
{
    // get proxy to send commands to the kinematic unit
    std::string kinematicUnitInstructionChannel = getProperty<std::string>("KinematicUnitName").getValue();
    kinematicUnitInterfacePrx = getProxy<KinematicUnitInterfacePrx>(kinematicUnitInstructionChannel);
    robotStatePrx = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    //kinObs = getProxy<ObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
    //ARMARX_INFO_S << "AdapterID: " << ArmarxGazeControl::getKinematicUnitObserver()->ice_getIdentity().name;
    
    yarp::os::Property options;
    options.put("name", "iKinGazeCtrl");
    if(!gazeCtrl.open(options))
        ARMARX_ERROR << "Cannot initialize YarpGazeControlHelper";
}


void ArmarxGazeControl::onDisconnectComponent()
{
    gazeCtrl.close();
}


void ArmarxGazeControl::onExitComponent()
{
    ARMARX_IMPORTANT<<"onExitComponent";
}


