/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpGazeControlHelper
 * @author     Ali Paikan ( ali dot paikan at iit dot it )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_YarpArmarxBridge_YarpGazeControlHelper_H
#define _ARMARX_COMPONENT_YarpArmarxBridge_YarpGazeControlHelper_H

#include <yarp/sig/all.h>
#include <yarp/os/Bottle.h>
#include <yarp/os/Port.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/RateThread.h>
#include <yarp/os/Stamp.h>
#include <yarp/os/Network.h>
#include <yarp/sig/Vector.h>
#include <yarp/os/Time.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

namespace armarx
{
    
    /*
     * XdPort
     */
    class XdPort : public  yarp::os::BufferedPort<yarp::os::Bottle> {
    public:
        virtual void onRead(yarp::os::Bottle &b);
    };

    /*
     * MonoPort
     */
    class MonoPort : public  yarp::os::BufferedPort<yarp::os::Bottle> {
    public:
        virtual void onRead(yarp::os::Bottle &b);
    };

    /*
     * StereoPort
     */
    class StereoPort : public  yarp::os::BufferedPort<yarp::os::Bottle> {
    public:
        virtual void onRead(yarp::os::Bottle &b);
    };


    /*
     * AnglesPort
     */
    class AnglesPort : public  yarp::os::BufferedPort<yarp::os::Bottle> {
    public:
        virtual void onRead(yarp::os::Bottle &b);
    };


    class YarpGazeControlHelper : public yarp::os::PortReader, yarp::os::RateThread {
    public:
        YarpGazeControlHelper();
        virtual ~YarpGazeControlHelper();
       
        virtual bool open(yarp::os::Property& config);

        virtual bool close(void);

        bool respond(yarp::os::Bottle &cmd, yarp::os::Bottle &reply);
        virtual bool read(yarp::os::ConnectionReader& connection);

        virtual void run(); 
        virtual bool threadInit();
        virtual void threadRelease();

    private:
        yarp::os::Property config;
        yarp::os::Stamp stamp;

        // input ports
        XdPort xdport;
        MonoPort monport;
        StereoPort steport;
        AnglesPort angport;
        yarp::os::Port rpcport;

        // reporter ports
        yarp::os::BufferedPort<yarp::sig::Vector> port_x;
        yarp::os::BufferedPort<yarp::sig::Vector> port_q;
        yarp::os::BufferedPort<yarp::os::Bottle> port_event;
        yarp::os::BufferedPort<yarp::sig::Vector> port_angles;

    };
}

#endif
