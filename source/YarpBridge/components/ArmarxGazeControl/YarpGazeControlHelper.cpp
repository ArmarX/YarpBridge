/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpGazeControlHelper
 * @author     Ali Paikan ( ali dot paikan at gmail dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <sstream>

#include "ArmarxGazeControl.h"
#include "YarpGazeControlHelper.h"

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>

using namespace armarx;
using namespace yarp::os;
using namespace yarp::sig;
using namespace std;

YarpGazeControlHelper::YarpGazeControlHelper() : RateThread(10)

{
}

YarpGazeControlHelper::~YarpGazeControlHelper()
{
    close();
}

bool YarpGazeControlHelper::open(yarp::os::Property& config) 
{
    string ctrlName = (config.check("name")) ? config.find("name").asString() : "iKinGazeCtrl";
    stringstream str;
    str << "/" << ctrlName << "/xd:i";
    bool ret = xdport.open(str.str().c_str());
    xdport.useCallback();

    str.str("");
    str << "/" << ctrlName << "/mono:i";
    ret &= monport.open(str.str().c_str());
    monport.useCallback();

    str.str("");
    str << "/" << ctrlName << "/stereo:i";
    ret &= steport.open(str.str().c_str());
    steport.useCallback();

    str.str("");
    str << "/" << ctrlName << "/angles:i";
    ret &= angport.open(str.str().c_str());
    angport.useCallback();

    str.str("");
    str << "/" << ctrlName << "/rpc";
    ret &= rpcport.open(str.str().c_str());
    if(ret)
        rpcport.setReader(*this);

    // start the thread
    if(ret) {
        int msrate = (config.check("rate")) ? config.find("rate").asInt() : 10;
        RateThread::setRate(msrate);
        ret &= RateThread::start();
    }

    return ret;
}

bool YarpGazeControlHelper::close(void)
{
    RateThread::stop();

    xdport.interrupt();
    xdport.close();
    monport.interrupt();
    monport.close();
    steport.interrupt();
    steport.close();
    angport.interrupt();
    angport.close();
    rpcport.close();

    return true;
}

bool YarpGazeControlHelper::read(yarp::os::ConnectionReader& connection) 
{
    yarp::os::Bottle cmd, response;
    if (!cmd.read(connection)) { return false; }
    printf("command received: %s\n", cmd.toString().c_str());

    bool result = respond(cmd,response);
    if (response.size()>=1) {
        yarp::os::ConnectionWriter *writer = connection.getWriter();
        if (writer!=0) {
            if (response.get(0).toString()=="many" && writer->isTextMode()) {
                for (int i=1; i<response.size(); i++) {
                    Value& v = response.get(i);
                    if (v.isList()) {
                        v.asList()->write(*writer);
                    } else {
                        Bottle b;
                        b.add(v);
                        b.write(*writer);
                    }
                }
            } else {
                response.write(*writer);
            }
            //printf("response sent: %s\n", response.toString().c_str());
        }
    }
    return result;                
}

bool YarpGazeControlHelper::threadInit() 
{
    string ctrlName = (config.check("name")) ? config.find("name").asString() : "iKinGazeCtrl";
    stringstream str;
    str << "/" << ctrlName << "/x:o";
    bool ret = port_x.open(str.str().c_str());

    str.str("");
    str << "/" << ctrlName << "/q:o";
    ret &= port_q.open(str.str().c_str());

    str.str("");
    str << "/" << ctrlName << "/angles:o";
    ret &= port_angles.open(str.str().c_str());

    str.str("");
    str << "/" << ctrlName << "/events:o";
    ret &= port_event.open(str.str().c_str());

    return ret;
}

void YarpGazeControlHelper::threadRelease() 
{
    port_x.close();
    port_q.close();
    port_angles.close();
    port_event.close();
}

void YarpGazeControlHelper::run()
{
    stamp.update();

    /* 
     * the actual fixation point (Vector of 3 double). Units in meters.     
     */
    Vector& fix = port_x.prepare();
    fix.clear();
    fix.resize(3);
    // get the data from armarx
    fix[0] = 1.0;
    fix[1] = 2.0;
    fix[2] = 3.0;
    port_x.setEnvelope(stamp);
    port_x.write();


    /* the actual joints configuration during movement (Vector of 9 double). 
     * The order for torso angles is the one defined by kinematic chain (reversed order). 
     * Useful in conjunction with the viewer. Units in degrees.
     */
    Vector& q = port_q.prepare();
    q.clear();
    q.resize(9);
    // get the data from armarx
    port_q.setEnvelope(stamp);
    port_q.write();


    /* the current azimuth/elevation couple wrt to the absolute head position,
     * together with the current vergence (Vector of 3 double). Units in degrees.
     */
    Vector& ang = port_angles.prepare();
    ang.clear();
    ang.resize(3);
    // get the data from armarx
    port_angles.setEnvelope(stamp);
    port_angles.write();

    /*
     * streams out the event associated to the controller's state.
     */
    Bottle &stat = port_event.prepare();
    stat.clear();
    stat.addString("busy");
    // add the current state (?)
    port_event.setEnvelope(stamp);
    port_event.write();
}

bool YarpGazeControlHelper::respond(yarp::os::Bottle &cmd, yarp::os::Bottle &reply)
{
    reply.clear();
    reply.addString("Hehe");
    return true;
}


void XdPort::onRead(Bottle &bt) 
{
    cout<<"XdPort Received "<<bt.toString()<<endl;
}

void MonoPort::onRead(Bottle &bt) 
{
    cout<<"MonoPort Received "<<bt.toString()<<endl;
}

void StereoPort::onRead(Bottle &bt) 
{
    cout<<"StereoPort Received "<<bt.toString()<<endl;
}

void AnglesPort::onRead(Bottle &bt) 
{
    cout<<"AnglesPort Received "<<bt.toString()<<endl;
}



