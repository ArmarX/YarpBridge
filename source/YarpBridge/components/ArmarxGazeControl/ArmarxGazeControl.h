/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    YarpArmarxBridge::ArmarXObjects::ArmarxGazeControl
 * @author     Ali Paikan ( ali dot paikan at iit dot com )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_MANAGEDICEOBJECT_YarpArmarxBridge_ArmarxGazeControl_H
#define _ARMARX_MANAGEDICEOBJECT_YarpArmarxBridge_ArmarxGazeControl_H


#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXCore/core/Component.h>

/* VirtualRobot headers */
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>

#include "YarpGazeControlHelper.h"

namespace armarx
{
    /**
     * @class ArmarxGazeControlPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class ArmarxGazeControlPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ArmarxGazeControlPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Robot", std::string("armarx"), "Robot name");
            //defineOptionalProperty<std::string>("Parts", "left_arm:Left ArmWithHand;right_arm:Right ArmWithHand;head:Head;torso:Hip;left_leg:Left Leg;right_leg:Right Leg", "List of robot part names (icubName:SimoxRobotNodeSetName)");
            defineRequiredProperty<std::string>("KinematicUnitName","Name of the KinematicUnit Proxy");
            //defineRequiredProperty<std::string>("PartsConfigPath","path to the robot parts configurations");
            defineOptionalProperty<std::string>("RobotStateComponentName","RobotStateComponent", "Name of the RobotStateComponent Proxy");
            defineOptionalProperty<std::string>("KinematicUnitObserverName","KinematicUnitObserver", "Name of the KinematicUnitObserver Proxy");
        }
    };


    /**
     * @class ArmarxGazeControl
     * @brief A brief description
     *
     * Detailed Description
     */
    class ArmarxGazeControl :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "ArmarxGazeControl";
        }

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent() override;

    private:      
        YarpGazeControlHelper gazeCtrl;

        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;        // send commands to kinematic unit
        RobotStateComponentInterfacePrx robotStatePrx;
        VirtualRobot::RobotPtr localRobot;
        ObserverInterfacePrx kinObs;

        std::string robotName;
        std::string robotNodeSetName;

        VirtualRobot::RobotPtr robot;
        VirtualRobot::RobotNodeSetPtr robotNodeSet;


    };
}

#endif
