armarx_component_set_name("YarpForceTorqueUnitApp")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    YarpForceTorqueUnit
    ${Simox_LIBRARIES}
)

set(EXE_SOURCE YarpForceTorqueUnitApp.h main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
