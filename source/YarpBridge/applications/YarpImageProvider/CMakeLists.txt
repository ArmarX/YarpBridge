armarx_component_set_name("YarpImageProviderApp")

find_package(IVT QUIET)
armarx_build_if(IVT_FOUND "ivt library not found")
if(IVT_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS YarpImageProvider ${Simox_LIBRARIES})

set(EXE_SOURCE YarpImageProviderApp.h main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
